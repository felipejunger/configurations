#!/bin/sh

# Store the directory the script is running from
LOC=$(readlink -f "$0")
DIR=$(dirname "$LOC")

# Change the appearance of the module identifier. if this is set to "unicode", then symbols will be used as identifiers instead of text. E.g. [📪 0] instead of [MAIL 0].
# Requires a font with adequate unicode character support
export IDENTIFIER="unicode"

dwm_date () {
        printf "%s" "$(date "+%a %d-%m-%y %T")"
        printf "%s\n"
}

# Update dwm status bar every second
while true
do
    xsetroot -name "$(dwm_date)"
    sleep 1
done
