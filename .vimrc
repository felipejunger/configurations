call plug#begin('~/.vim/plugins')
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neoclide/coc.nvim', {'tag': '*', 'branch': 'release'}
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
Plug 'rhysd/vim-clang-format'
Plug 'tpope/vim-dispatch' 
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'sheerun/vim-polyglot'
Plug 'tomasiser/vim-code-dark'
call plug#end()

syntax on
colorscheme codedark
let mapleader = ","
set shell=/bin/bash
set cursorline
filetype on
filetype indent on
filetype plugin indent on
set title
set number
set expandtab
set tabstop=4
set shiftwidth=4
set laststatus=2
set noshowmode
set t_Co=256
set smarttab
set lbr
set tw=150
set ai
set smartindent
set wildmenu
set wildignore=*.o,*~
set ww+=<,>
set mouse=a
if has("win32")
        set wildignore+=.git\*,*.meta
else
        set wildignore+=*/.git/*,*.meta
endif
set noerrorbells
set novisualbell
set bs=2
set foldmethod=manual
set ttimeoutlen=1
set hidden
set nobackup
set nowritebackup
set cmdheight=1
set updatetime=300
set shortmess+=c
set signcolumn=yes
set encoding=utf-8
set fileencoding=utf-8
set belloff+=ctrlg
set completeopt-=menu,preview
set completeopt+=noinsert,noselect,longest,menuone
set showtabline=1  " Show tabline

" Use different cursor shape for each mode
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"
" Navigation stuff
nmap <silent> <leader>k :wincmd k<CR>
nmap <silent> <leader>j :wincmd j<CR>
nmap <silent> <leader>h :wincmd h<CR>
nmap <silent> <leader>l :wincmd l<CR>
inoremap <C-k> <C-o>gk
inoremap <C-h> <Left>
inoremap <C-l> <Right>
inoremap <C-j> <C-o>gj
nnoremap <silent><leader>o :tabprevious<CR>
nnoremap <silent><leader>p :tabnext<CR>

nnoremap <leader>t :tabe<CR>
nmap <silent> <leader>vs :vsplit <CR>
nmap <silent> <leader>hs :split <CR>
nmap <silent> <C-l> :ls<CR>
map  <silent> <C-n> :NERDTreeToggle<CR>
nmap <silent> <C-p> :Files <CR>
nmap <silent> <C-t> :Tags <CR>
tnoremap <silent> <ESC><ESC> : <C-\><C-n>
vnoremap <leader>Y "*y
vnoremap <leader>P "*p
vnoremap <leader>y "+y
vnoremap <leader>p "+p
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

nmap <silent> df <Plug>(coc-definition)

autocmd FileType c,cpp,h nnoremap <silent><buffer><C-K> :<C-u>ClangFormat<CR>

let NERDTreeQuitOnOpen = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

let g:lightline = {
                        \ 'colorscheme': 'wombat',
                        \ 'active': {
                        \   'left': [ [ 'mode', 'paste' ],
                        \             [ 'gitbranch','readonly', 'filename', 'modified' ] ]
                        \ },
                        \ 'component_function': {
                        \   'gitbranch': 'fugitive#head',
                        \ }
                        \}

let g:lightline.separator = {
                        \   'left': '', 'right': ''
                        \}
let g:lightline.subseparator = {
                        \   'left': '', 'right': '' 
                        \}

let g:lightline.tabline = {
  \   'left': [ ['tabs'] ],
 \   'right': [ [''] ]
  \ }

autocmd FileType cpp set makeprg=clang++\ -o\ %<\ %
autocmd FileType cpp nnoremap <leader>cc :make && ./%<<CR>
