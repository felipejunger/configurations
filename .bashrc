#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

export XKB_DEFAULT_LAYOUT=br
export XKB_DEFAULT_OPTIONS=altwin

# If running from tty1 start sway
if [ "$(tty)" = "/dev/tty1" ]; then
	exec startx
fi

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

alias o="xdg-open $@"
